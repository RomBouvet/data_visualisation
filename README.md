# Ventes de jeux vidéo

* **Quoi** : Projet de visualisation de données
* **Pourquoi** : Pour la validation d'une matière pour mon master informatique IA
* **Où** : Université de Reims Champagne Ardenne - Reims
* **Quand** : Fev-Mar 2019

## Installation

* Avoir [R](https://www.r-project.org/) installé
* Installer les libraries nécessaires

```
install.packages('shiny')
install.packages('shinydashboard')
install.packages('tidyverse')
install.packages('ggplot2')
install.packages('wordcloud2')
install.packages('leaflet')
```

* Télécharger le dossier ou `git clone https://gitlab.com/RomBouvet/data_visualisation`
* Exécuter le script app.R

## Auteurs

* **Romain BOUVET** - [GitLab](https://gitlab.com/RomBouvet/) - [LinkedIn](https://www.linkedin.com/in/romain-bouvet-0a977312b/)
* **Jérémy CORPET** - [GitLab](https://gitlab.com/Manhari)